FROM maven:3-jdk-8-alpine
WORKDIR /app
COPY . /app
RUN mvn package
CMD ["java","-jar","target/hello-world-0.0.1-SNAPSHOT.jar"]
